FROM python:3.6-slim

WORKDIR /api

COPY requirements.txt requirements.txt

RUN apt-get update && apt-get install -y \
    libpq-dev \
    postgresql

RUN pip install -r requirements.txt

COPY . .

# Set Flask's environment variable
ENV FLASK_APP=server
ENV PORT=5000
ENV USE_LOCAL_DB="true"
ENV DB_USER="<db-user>"
ENV DB_PASS="<db-pass>"
ENV DB_URL="<db-url>"
ENV DB_NAME="hardtrace"
ENV VISUALIZER_USER="<visualizer-user>"
ENV VISUALIZER_TOKEN="<visualizer-token>"

# Uncomment the line below if the used databse has not been initialized yet
#RUN flask init

# Start serving the API with gunicorn. "server:create_app" obtains the Flask app from __init__.py
CMD exec gunicorn --bind :$PORT --workers 1 --threads 4 --timeout 0 "server:create_app()"
