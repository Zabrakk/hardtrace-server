# Network Data
In addition to summaries of sessions, this API is also able to provide Hardtrace session information in a format that is suitable for network visualization.

## An Example
_**Note:** Look into the file **Summarization.md** before reading this._

Continuing from the Summarization example, by sending a GET request to <**API_IP:Port**>**/api/analysis/summary/** with the following JSON contents:
```json
{
    "graph": true, // Only difference is here
    "includes": "users",
    "summarize": [
        {
            "user": "user1",
            "session": "test"
        }
        // Note: Additional users can be added here
    ]
}
```
network graph data can be obtained from the API. 

With the commands run in the Summarization example, the obtained data will look approximately like this:
```json
{
    "nodes": [
        {
            "id": 0,
            "name": "test.txt",
            "created_at": "2022-05-30 15:34:40",
            "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2",
            "inode": 15337759,
            "source_node": true,
            "selected_file": false
        },
        {
            "id": 1,
            "name": "test2.txt",
            "created_at": "2022-05-30 15:34:49",
            "hash": "bf0a8a129331f72cf7e55d2dbfe9dbdeed59ff2851e7ed18c3e498c53edb7ad5",
            "inode": 15337760,
            "source_node": false,
            "selected_file": false
        },
        {
            "id": 2,
            "name": null,
            "created_at": null,
            "hash": null,
            "inode": null,
            "source_node": true,
            "selected_file": false
        },
        {
            "id": 3,
            "name": null,
            "created_at": null,
            "hash": null,
            "inode": null,
            "source_node": true,
            "selected_file": false
        }
    ],
    "edges": [
        {
            "cmd": "nano test.txt",
            "short_cmd": "nano",
            "source": 2,
            "target": 0,
            "run_by": "user1"
        },
        {
            "cmd": "grep --color=auto test test.txt -H",
            "short_cmd": "grep",
            "source": 0,
            "target": 1,
            "run_by": "user1"
        },
        {
            "cmd": "cat test2.txt",
            "short_cmd": "cat",
            "source": 1,
            "target": 3,
            "run_by": "user1"
        }
    ]
}
```
The data divides syscalls recorded by Hardtrace so that accessed files are provided as the networks nodes, and ran commands as the edges connecting nodes to each other.
