import os
import pytest
from sqlalchemy.exc import IntegrityError, StatementError

from server import create_app, db
from server.utilities import to_datetime
from server.models.user import User
from server.models.session import Session
from server.models.process import Process
from server.models.syscall import Syscall
from server.models.accessed_file import AccessedFile

from tests.constants import test_config

# Tests are run with $ python3 -m pytest -s tests
@pytest.fixture
def app():
    """
    Create a dummy database for testing
    """
    if os.path.exists("tests/test.db"):
        os.remove("tests/test.db")
    # Create the app with the above configuration
    app = create_app(test_config)
    # Create the database
    with app.app_context():
        db.create_all()
    yield app

def get_user(token='test_token', pseudonym='test_user', created_at='2021-06-17 11:18:53') -> User:
    return User(
        auth_token=token,
        pseudonym=pseudonym,
        created_at=to_datetime(created_at)
    )

def get_session(name='test_session', created_at='2021-06-17 11:23:13') -> Session:
    return Session(
        name=name,
        created_at=to_datetime(created_at)
    )

def get_accessed_file(created_at='2021-06-17 14:02:22', id=0, filepath="/testing/test_file.txt", pipe=False, inode=0, hash="abcdefg") -> AccessedFile:
    return AccessedFile(
        created_at=to_datetime(created_at),
        hardtrace_id=id,
        filepath=filepath,
        pipe=pipe,
        inode=inode,
        hash=hash
    )

def get_process(created_at='2021-06-17 14:02:22', id=0, pid=1) -> Process:
    return Process(
        created_at=to_datetime(created_at),
        hardtrace_id=id,
        pid=pid
    )

def get_syscall(created_at='2021-06-17 14:02:22', name="EXECVE", arg0='bash', args=None) -> Syscall:
    return Syscall(
        created_at=to_datetime(created_at),
        name=name,
        arg0=arg0,
        args=args
    )

def raises_integrity_err(data):
    db.session.add(data)
    with pytest.raises(IntegrityError):
        db.session.commit()
    db.session.rollback()

def raises_statement_err(data):
    db.session.add(data)
    with pytest.raises(StatementError):
        db.session.commit()
    db.session.rollback()

def test_creation(app):
    """
    Test the basic creation on storing of the different models
    """
    print('\n================RUNNING DATABASE TESTS================')
    print('Testing basic instance and relation creation: ', end='')
    with app.app_context():
        # Create objects
        user = get_user()
        session = get_session()
        accessed_file = get_accessed_file()
        syscall = get_syscall()
        process1 = get_process()
        process2 = get_process(id=1, pid=2)
        # Add connections between the objects
        user.sessions.append(session)
        session.user = user
        session.accessed_files.append(accessed_file)
        session.syscalls.append(syscall)
        session.processes.append(process1)
        session.processes.append(process2)
        accessed_file.session = session
        syscall.session = session
        process1.session = session
        process2.session = session
        syscall.process = process1
        syscall.file = accessed_file
        process2.parent = process1
        # Stage changes to db
        db.session.add(user)
        db.session.add(session)
        db.session.add(accessed_file)
        db.session.add(syscall)
        db.session.add(process1)
        db.session.add(process2)
        # Commit changes
        db.session.commit()
        
        # Test that everything is stored correctly
        assert User.query.count() == 1
        assert Session.query.count() == 1
        assert AccessedFile.query.count() == 1
        assert Syscall.query.count() == 1
        assert Process.query.count() == 2

        # Check relations
        db_user = User.query.first()
        db_session = Session.query.first()
        db_accessed_file = AccessedFile.query.first()
        db_syscall = Syscall.query.first()
        db_process1 = Process.query.first()
        db_process2 = Process.query.filter_by(pid=2).first()
        
        assert db_session in db_user.sessions
        assert db_session.user == db_user
        assert db_accessed_file in db_session.accessed_files
        assert db_session.accessed_files == [db_accessed_file]
        assert db_syscall in db_session.syscalls
        assert db_session.syscalls == [db_syscall]
        assert db_process1 in db_session.processes
        assert db_process2 in db_session.processes
        assert db_session.processes == [db_process1, db_process2]
        assert db_syscall.file == accessed_file
        assert db_syscall.process == db_process1
        assert db_process2.parent == db_process1

        # Check other values
        # User
        assert db_user.auth_token == user.auth_token
        assert db_user.created_at == user.created_at
        assert db_user.sessions == user.sessions
        # Session
        assert db_session.created_at == session.created_at
        assert db_session.name == session.name
        # AccessedFile
        assert db_accessed_file.created_at ==  accessed_file.created_at
        assert db_accessed_file.filepath ==  accessed_file.filepath
        assert db_accessed_file.pipe ==  accessed_file.pipe
        assert db_accessed_file.inode ==  accessed_file.inode
        assert db_accessed_file.hash ==  accessed_file.hash
        # Syscall
        assert db_syscall.name == syscall.name
        assert db_syscall.arg0 == syscall.arg0
        assert db_syscall.args == syscall.args
        # Process
        assert db_process1.pid == process1.pid
        assert db_process2.pid == process2.pid


def test_user(app):
    print('\nTesting User model: ', end='')
    with app.app_context():
        # Invalid values
        user = get_user(token=None)
        raises_integrity_err(user)      

        user = get_user()
        user.created_at = None
        raises_integrity_err(user)
        user.created_at = "test"
        raises_statement_err(user)

        user = get_user()
        key_violation = get_process()
        with pytest.raises(KeyError):
            user.sessions.append(key_violation)

        # Uniqueness
        user = get_user()
        user2 = get_user(pseudonym='test_user2')
        db.session.add(user)
        raises_integrity_err(user2)

        user2.pseudonym = 'test_user'
        db.session.add(user)
        raises_integrity_err(user2)

        user2.pseudonym = 'test_user2'
        user2.auth_token = 'test_token2'
        db.session.add(user)
        db.session.add(user2)
        db.session.commit()
        assert User.query.count() == 2

def test_session(app):
    print('\nTesting Session model: ', end='')
    with app.app_context():
        # Invalid values
        session = get_session(name=None)
        raises_integrity_err(session)

        session = get_session()
        session.created_at = None
        raises_integrity_err(session)
        session.created_at = "test"
        raises_statement_err(session)

        session = get_session()
        key_violation1 = get_process()
        key_violation2 = get_user()
        with pytest.raises(KeyError):
            session.user = key_violation1
        with pytest.raises(KeyError):
            session.accessed_files.append(key_violation2)
        with pytest.raises(KeyError):
            session.syscalls.append(key_violation2)
        with pytest.raises(KeyError):
            session.processes.append(key_violation2)

        # Uniqueness
        user = get_user()
        session1 = get_session()
        session2 = get_session()

        session1.user = user
        session2.user = user
        db.session.add(session1)
        raises_integrity_err(session2)

        session2.name = "test_session2"
        db.session.add(session1)
        db.session.add(session2)
        assert Session.query.count() == 2

def test_accessed_file(app):
    print('\nTesting AccessedFile model: ', end='')
    with app.app_context():
        file = get_accessed_file()
        file.created_at = None
        raises_integrity_err(file)
        file.created_at = "test"
        raises_statement_err(file)

        file = get_accessed_file(id=None)
        raises_integrity_err(file)

        file = get_accessed_file(filepath=None)
        raises_integrity_err(file)

        file = get_accessed_file(pipe=None)
        raises_integrity_err(file)
        file.pipe = 'test'
        raises_statement_err(file)

        file = get_accessed_file(inode=None)
        raises_integrity_err(file)

        file = get_accessed_file(hash=None)
        raises_integrity_err(file)

def test_process(app):
    print('\nTesting Process model: ', end='')
    with app.app_context():
        process = get_process()
        process.created_at = None
        raises_integrity_err(process)
        process.created_at = "test"
        raises_statement_err(process)

        process = get_process(id=None)
        raises_integrity_err(process)

        process = get_process(pid=None)
        raises_integrity_err(process)

def test_syscall(app):
    print('\nTesting Syscall model: ', end='')
    with app.app_context():
        syscall = get_syscall()
        syscall.created_at = None
        raises_integrity_err(syscall)
        syscall.created_at = "test"
        raises_statement_err(syscall)

        syscall = get_syscall(name=None)
        raises_integrity_err(syscall)

        syscall = get_syscall(arg0=None)
        raises_integrity_err(syscall)

        syscall = get_syscall()
        key_violation = get_user()
        with pytest.raises(KeyError):
            syscall.session = key_violation
