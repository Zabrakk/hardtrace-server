import pytest
from sqlalchemy import event
from sqlalchemy.engine import Engine

from server import create_app
from server import db as test_db
from server.auth import auth

from tests.constants import *
from tests.utilities import *

AUTH = {'Username': auth.visualizer_user, 'X-Authorization': auth.visualizer_token}

# Tests are run with $ python3 -m pytest -s -v tests

@event.listens_for(Engine, 'connect')
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute('PRAGMA foreign_keys=ON')
    cursor.close()

@pytest.fixture
def app():
    app = create_app(analysis_test_config)
    with app.app_context():
        yield app

@pytest.fixture
def db(app):
    with app.app_context():
        yield test_db

@pytest.fixture
def client(app, db):
    with app.app_context():
        c = Client(app.test_client(), db)
        yield c

def test_user_info(client: Client):
    print('\n================RUNNING API ANALYSIS TESTS================')
    print(f'Testing GET for {USER_INFO_URL} ', end='')
    json = {
        'user1': [ 'test' ],
        'user2': [ 'test' ]
    }
    get(client.fc, USER_INFO_URL, 200, json=json, auth=AUTH)

def test_file_info(client: Client):
    print(f'\nTesting GET for {FILE_INFO_URL} ', end='')
    get(client.fc, FILE_INFO_URL, 200, json=FILE_INFO_RESULT, auth=AUTH)

def test_session_summary(client: Client):
    print('\nTesting session summary creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=USER_SUMMARY_REQUEST_JSON, headers=AUTH)
    assert r.json == USER_SUMMARY_JSON

def test_session_network(client: Client):
    print('\nTesting session network creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=USER_NETWORK_REQUEST_JSON, headers=AUTH)
    assert r.json == USER_NETWORK_JSON

def test_session_summary_ignore(client: Client):
    print('\nTesting session summary ignore creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=USER_SUMMARY_IGNORE_REQUEST_JSON, headers=AUTH)
    assert r.json == USER_SUMMARY_IGNORE_JSON

def test_session_ignore_network(client: Client):
    print('\nTesting session network ignore creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=USER_NETWORK_IGNORE_REQUEST_JSON, headers=AUTH)
    assert r.json == USER_NETWORK_IGNORE_JSON

def test_file_summary(client: Client):
    print('\nTesting file summary creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=FILE_SUMMARY_REQUEST_JSON, headers=AUTH)
    assert r.json == FILE_SUMMARY_JSON

def test_file_network(client: Client):
    print('\nTesting file network creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=FILE_NETWORK_REQUEST_JSON, headers=AUTH)
    assert r.json == FILE_NETWORK_JSON

def test_file_ignore_summary(client: Client):
    print('\nTesting file ignore summary creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=FILE_SUMMARY_IGNORE_REQUEST_JSON, headers=AUTH)
    assert r.json == FILE_SUMMARY_IGNORE_JSON

def test_file_ignore_network(client: Client):
    print('\nTesting file network ignore creation ', end='')
    r = client.fc.post(ANALYSIS_URL, json=FILE_NETWORK_IGNORE_REQUEST_JSON, headers=AUTH)
    assert r.json == FILE_NETWORK_IGNORE_JSON
