test_config = {
    "SQLALCHEMY_DATABASE_URI": "sqlite:///../tests/test.db",
    "SQLALCHEMY_TRACK_MODIFICATIONS": False,
    "TESTING": True
}

analysis_test_config = {
    "SQLALCHEMY_DATABASE_URI": "sqlite:///../tests/analysis_test.db",
    "SQLALCHEMY_TRACK_MODIFICATIONS": False,
    "TESTING": True
}

TEST_USER = "test_user"
TEST_TOKEN = "a"*32
TEST_TOKEN2 = "b"*32
TEST_SESSION = "test_session"

# URLS
STATUS_URL = "/api/status/"
CREATE_USER_URL = "/api/create-user/"
USER_INFO_URL = "/api/analysis/user-info/"
FILE_INFO_URL = "/api/analysis/file-info/"
SESSION_URL = f"/api/users/{TEST_USER}/sessions/"
SESSION_ITEM_URL = f"/api/users/{TEST_USER}/sessions/{TEST_SESSION}/"
ACCESSED_FILE_URL = f"/api/users/{TEST_USER}/sessions/{TEST_SESSION}/accessed-files/"
PROCESS_URL = f"/api/users/{TEST_USER}/sessions/{TEST_SESSION}/processes/"
SYSCALL_URL = f"/api/users/{TEST_USER}/sessions/{TEST_SESSION}/syscalls/"
ANALYSIS_URL = "api/analysis/summary/"

# JSONS
ACCESSED_FILE_JSON = [
    {
        "id": 1,
        "created_at": "2021-06-16 23:29:26",
        "filepath": "/test/test_file.txt",
        "pipe": False,
        "inode": 123,
        "hash": "lfdsjvhdjbsfgsdar762387rad"
    },
    {
        "id": 2,
        "created_at": "2021-06-16 23:29:26",
        "filepath": "/test/test_file2.txt",
        "pipe": True,
        "inode": 12345,
        "hash": "dsvdfbvlskdc2sadgsd387rad"
    },
    {
        "id": 3,
        "created_at": "2021-06-16 23:29:26",
        "filepath": "/test/test_file3.txt",
        "pipe": False,
        "inode": 321,
        "hash": "dfgvbdfhbrtyhydzvsdcvsf"
    }
]

PROCESS_JSON = [
    {
        "id": 1,
        "created_at": "2021-06-16 23:29:26",
        "pid": 1,
        "parent_id": 0
    },
    {
        "id": 2,
        "created_at": "2021-06-16 23:30:26",
        "pid": 2,
        "parent_id": 1
    },
    {
        "id": 3,
        "created_at": "2021-06-16 23:30:26",
        "pid": 3,
        "parent_id": 2
    },
    {
        "id": 4,
        "created_at": "2021-06-16 23:30:26",
        "pid": 4,
        "parent_id": 0
    }
]

SYSCALL_JSON = [
    {
        "created_at": "2021-06-16 23:29:26",
        "name": "WRITE",
        "file_id": 0,
        "process_id": 0,
        "arg0": "bash",
        "args": "nano test.txt"
    },
    {
        "created_at": "2021-06-16 23:29:26",
        "name": "EXECVE",
        "file_id": 2,
        "process_id": 1,
        "arg0": "touch"
    },
    {
        "created_at": "2021-06-16 23:29:26",
        "name": "READ",
        "file_id": 1,
        "process_id": 0,
        "arg0": "cat test.txt"
    }
]

USER_SUMMARY_REQUEST_JSON = {
    "graph": False,
    "includes": "users",
    "summarize": [
        {
            "user": "user1",
            "session": "test"
        },
        {
            "user": "user2",
            "session": "test"
        }
    ]
}

USER_SUMMARY_IGNORE_REQUEST_JSON = {
    "graph": False,
    "includes": "users",
    "summarize": [
        {
            "user": "user1",
            "session": "test"
        },
        {
            "user": "user2",
            "session": "test"
        }
    ],
    "ignore-files": "file2*"
}

USER_SUMMARY_JSON = [
    {
        "pseudonym": "user1",
        "session": "test",
        "process_summary": [
            {
                "cmd": "bash",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "basename lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dirname lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dircolors -b",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "nano",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:42",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:45:41",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:45:41",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032915,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            }
        ]
    },
    {
        "pseudonym": "user2",
        "session": "test",
        "process_summary": [
            {
                "cmd": "bash",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:06",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "basename lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dirname lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dircolors -b",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "nano file1.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:13",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:21",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ],
                "new_files": []
            },
            {
                "cmd": "nano file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:26",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "file file2.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:36",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": []
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:44",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:44",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032923,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            },
            {
                "cmd": "grep --color=auto test file3.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:50",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:44",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032923,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ],
                "new_files": []
            }
        ]
    }
]

USER_SUMMARY_IGNORE_JSON = [
    {
        "pseudonym": "user1",
        "session": "test",
        "process_summary": [
            {
                "cmd": "bash",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "basename lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dirname lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dircolors -b",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:40",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "nano",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:42",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:45:41",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:45:41",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032915,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            }
        ]
    },
    {
        "pseudonym": "user2",
        "session": "test",
        "process_summary": [
            {
                "cmd": "bash",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:06",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "basename lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dirname lesspipe",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "dircolors -b",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:07",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "nano file1.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:13",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:21",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ],
                "new_files": []
            },
            {
                "cmd": "nano file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:26",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "file file2.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:36",
                "old_files": [],
                "new_files": []
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:44",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:44",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032923,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            },
            {
                "cmd": "grep --color=auto test file3.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-09 16:46:50",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:44",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032923,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ],
                "new_files": []
            }
        ]
    }
]

USER_NETWORK_REQUEST_JSON = {
    "graph": True,
    "includes": "users",
    "summarize": [
        {
            "user": "user1",
            "session": "test"
        },
        {
            "user": "user2",
            "session": "test"
        }
    ]
}

USER_NETWORK_IGNORE_REQUEST_JSON = {
    "graph": True,
    "includes": "users",
    "summarize": [
        {
            "user": "user1",
            "session": "test"
        },
        {
            "user": "user2",
            "session": "test"
        }
    ],
    "ignore-files": "file2*"
}

USER_NETWORK_JSON = {
    "nodes": [
        {
            "id": 0,
            "name": "file1.txt",
            "created_at": "2022-05-09 16:44:51",
            "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2",
            "inode": 6032913,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 1,
            "name": "file2.txt",
            "created_at": "2022-05-09 16:45:00",
            "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4",
            "inode": 6032914,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 2,
            "name": "file3.txt",
            "created_at": "2022-05-09 16:45:41",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe",
            "inode": 6032915,
            "source_node": False,
            "selected_file": False
        },
        {
            "id": 3,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 4,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 5,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 6,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 7,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        }
    ],
    "edges": [
        {
            "cmd": "user1: nano<$%&>user2: nano file1.txt",
            "short_cmd": "nano",
            "source": 3,
            "target": 0,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "user1: nano<$%&>user2: nano file2.txt",
            "short_cmd": "nano",
            "source": 4,
            "target": 1,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt file2.txt",
            "short_cmd": "cat",
            "source": 0,
            "target": 2,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt file2.txt",
            "short_cmd": "cat",
            "source": 1,
            "target": 2,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt",
            "short_cmd": "cat",
            "source": 0,
            "target": 5,
            "run_by": "user2"
        },
        {
            "cmd": "file file2.txt",
            "short_cmd": "file",
            "source": 1,
            "target": 6,
            "run_by": "user2"
        },
        {
            "cmd": "grep --color=auto test file3.txt",
            "short_cmd": "grep",
            "source": 2,
            "target": 7,
            "run_by": "user2"
        }
    ]
}

USER_NETWORK_IGNORE_JSON = {
    "nodes": [
        {
            "id": 0,
            "name": "file1.txt",
            "created_at": "2022-05-09 16:44:51",
            "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2",
            "inode": 6032913,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 1,
            "name": "file3.txt",
            "created_at": "2022-05-09 16:45:41",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe",
            "inode": 6032915,
            "source_node": False,
            "selected_file": False
        },
        {
            "id": 2,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 3,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 4,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        }
    ],
    "edges": [
        {
            "cmd": "user1: nano<$%&>user2: nano file1.txt",
            "short_cmd": "nano",
            "source": 2,
            "target": 0,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt file2.txt",
            "short_cmd": "cat",
            "source": 0,
            "target": 1,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt",
            "short_cmd": "cat",
            "source": 0,
            "target": 3,
            "run_by": "user2"
        },
        {
            "cmd": "grep --color=auto test file3.txt",
            "short_cmd": "grep",
            "source": 1,
            "target": 4,
            "run_by": "user2"
        }
    ]
}

FILE_SUMMARY_REQUEST_JSON = {
    "graph": False,
    "includes": "files",
    "summarize": [
        {
            "filename": "file3.txt",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
        }
    ]
}

FILE_SUMMARY_IGNORE_REQUEST_JSON = {
    "graph": False,
    "includes": "files",
    "summarize": [
        {
            "filename": "file3.txt",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
        }
    ],
    "ignore-files": "file1.txt"
}

FILE_SUMMARY_JSON = [
    {
        "pseudonym": "user1",
        "session": "test",
        "process_summary": [
            {
                "cmd": "nano",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:42",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "nano",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:42",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:45:41",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:44:51",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032913,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:45:41",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032915,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            }
        ]
    },
    {
        "pseudonym": "user2",
        "session": "test",
        "process_summary": [
            {
                "cmd": "nano file1.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:13",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ]
            },
            {
                "cmd": "nano file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:26",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:44",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:18",
                        "filepath": "file1.txt",
                        "pipe": False,
                        "inode": 6032921,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    },
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:44",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032923,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            }
        ]
    }
]

FILE_SUMMARY_IGNORE_JSON = [
    {
        "pseudonym": "user1",
        "session": "test",
        "process_summary": [
            {
                "cmd": "nano",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:44:42",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:45:41",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:45:00",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032914,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:45:41",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032915,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            }
        ]
    },
    {
        "pseudonym": "user2",
        "session": "test",
        "process_summary": [
            {
                "cmd": "nano file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:26",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ]
            },
            {
                "cmd": "cat file1.txt file2.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-09 16:46:44",
                "old_files": [
                    {
                        "created_at": "2022-05-09 16:46:29",
                        "filepath": "file2.txt",
                        "pipe": False,
                        "inode": 6032922,
                        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-09 16:46:44",
                        "filepath": "file3.txt",
                        "pipe": False,
                        "inode": 6032923,
                        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
                    }
                ]
            }
        ]
    }
]

FILE_NETWORK_REQUEST_JSON = {
    "graph": True,
    "includes": "files",
    "summarize": [
        {
            "filename": "file3.txt",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
        }
    ]
}

FILE_NETWORK_IGNORE_REQUEST_JSON = {
    "graph": True,
    "includes": "files",
    "summarize": [
        {
            "filename": "file3.txt",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
        }
    ],
    "ignore-files": "file1.txt",
}

FILE_NETWORK_JSON = {
    "nodes": [
        {
            "id": 0,
            "name": "file1.txt",
            "created_at": "2022-05-09 16:44:51",
            "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2",
            "inode": 6032913,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 1,
            "name": "file2.txt",
            "created_at": "2022-05-09 16:45:00",
            "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4",
            "inode": 6032914,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 2,
            "name": "file3.txt",
            "created_at": "2022-05-09 16:45:41",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe",
            "inode": 6032915,
            "source_node": False,
            "selected_file": True
        },
        {
            "id": 3,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 4,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        }
    ],
    "edges": [
        {
            "cmd": "user1: nano<$%&>user2: nano file1.txt",
            "short_cmd": "nano",
            "source": 3,
            "target": 0,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "user1: nano<$%&>user2: nano file2.txt",
            "short_cmd": "nano",
            "source": 4,
            "target": 1,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt file2.txt",
            "short_cmd": "cat",
            "source": 0,
            "target": 2,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt file2.txt",
            "short_cmd": "cat",
            "source": 1,
            "target": 2,
            "run_by": "user1 & user2"
        }
    ]
}

FILE_NETWORK_IGNORE_JSON = {
    "nodes": [
        {
            "id": 0,
            "name": "file2.txt",
            "created_at": "2022-05-09 16:45:00",
            "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4",
            "inode": 6032914,
            "source_node": True,
            "selected_file": False
        },
        {
            "id": 1,
            "name": "file3.txt",
            "created_at": "2022-05-09 16:45:41",
            "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe",
            "inode": 6032915,
            "source_node": False,
            "selected_file": True
        },
        {
            "id": 2,
            "name": None,
            "created_at": None,
            "hash": None,
            "inode": None,
            "source_node": True,
            "selected_file": False
        }
    ],
    "edges": [
        {
            "cmd": "user1: nano<$%&>user2: nano file2.txt",
            "short_cmd": "nano",
            "source": 2,
            "target": 0,
            "run_by": "user1 & user2"
        },
        {
            "cmd": "cat file1.txt file2.txt",
            "short_cmd": "cat",
            "source": 0,
            "target": 1,
            "run_by": "user1 & user2"
        }
    ]
}

# RESPONSE SCHEMAS
CREATE_USER_SCHEMA = {
    "type": "object",
    "minProperties": 2,
    "maxProperties": 2,
    "required": ["pseudonym", "auth_token"],
    "properties": {
        "pseudonym": {
            "type": "string",
            "pattern": "user[0-9]+"
        },
        "auth_token": {
            "type": "string",
            "minLength": 32
        }
    }
}

FILE_INFO_RESULT = [
    {
        "created_at": "2022-05-09 16:44:51",
        "filepath": "file1.txt",
        "pipe": False,
        "inode": 6032913,
        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
    },
    {
        "created_at": "2022-05-09 16:45:00",
        "filepath": "file2.txt",
        "pipe": False,
        "inode": 6032914,
        "hash": "98ea6e4f216f2fb4b69fff9b3a44842c38686ca685f3f55dc48c5d3fb1107be4"
    },
    {
        "created_at": "2022-05-09 16:45:41",
        "filepath": "file3.txt",
        "pipe": False,
        "inode": 6032915,
        "hash": "5f731a3662efb530dde4a87247c4d653c73e7aa2f459a9bba6b3a64024d5eefe"
    }
]
