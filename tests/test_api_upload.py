import os
import copy
import pytest
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from flask.testing import FlaskClient
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import event
from sqlalchemy.engine import Engine

from server import create_app
from server import db as test_db
from server.utilities import to_datetime
# Database models
from server.models.user import User
from server.models.session import Session
from server.models.accessed_file import AccessedFile
from server.models.process import Process
from server.models.syscall import Syscall

from tests.constants import *
from tests.utilities import *


@event.listens_for(Engine, 'connect')
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute('PRAGMA foreign_keys=ON')
    cursor.close()

@pytest.fixture
def app():
    app = create_app(test_config)
    with app.app_context():
        yield app

@pytest.fixture
def db(app):
    """
    Create a dummy database for testing
    """
    if os.path.exists("tests/test.db"):
        os.remove("tests/test.db")
    with app.app_context():
        test_db.create_all()
        populate_test_db(test_db)
    yield test_db

@pytest.fixture
def client(app, db):
    with app.app_context():
        c = Client(app.test_client(), db)
        yield c


def populate_test_db(db: SQLAlchemy):
    user = User(pseudonym=TEST_USER, auth_token=TEST_TOKEN, created_at=to_datetime('2021-08-03 11:46:22'))
    user2 = User(pseudonym=TEST_USER+'2', auth_token=TEST_TOKEN2, created_at=to_datetime('2021-08-03 11:46:22'))
    session1 = Session(name=TEST_SESSION, created_at=to_datetime('2021-08-03 11:46:22'))
    session2 = Session(name=TEST_SESSION+'2', created_at=to_datetime('2021-08-03 11:46:22'))
    user.sessions.append(session1)
    user.sessions.append(session2)
    session1.user = user
    session2.user = user

    db.session.add(user)
    db.session.add(user2)
    db.session.add(session1)
    db.session.add(session2)
    db.session.commit()


def test_get_status(client: Client):
    print('\n================RUNNING API UPLOAD TESTS================')
    print(f'Testing GET for {STATUS_URL} ', end='')
    get(client.fc, STATUS_URL, 200)

def test_create_user(client: Client):
    print(f'\nTesting GET for {CREATE_USER_URL} ', end='')
    old_num_of_users = User.query.count()
    auth = {'pseudonym': os.getenv('CREATE_USER_USERNAME'), 'X-Authorization': os.getenv('CREATE_USER_TOKEN')}
    get(client.fc, CREATE_USER_URL, 200, schema=CREATE_USER_SCHEMA, auth=auth)
    assert User.query.count() > old_num_of_users

def test_create_and_get_session(client: Client):
    print(f'\nTesting POST for {SESSION_URL} ', end='')
    json = {'name': 'test session'}
    post(client.fc, SESSION_URL, json, 201)
    
    # Test that session was added by sending GET to its session item
    get(client.fc, SESSION_URL+'test session/', 200)

def test_incorrect_get_session(client: Client):
    print(f'\nTesting incorrect GET for {SESSION_URL} ', end='')
    # Incorrect authorization
    auth = {'X-Authorization': 'TEST_TOKEN'}
    get(client.fc, SESSION_URL, 403, auth=auth)

    # Missing authorization
    get(client.fc, SESSION_URL, 403, auth=None)

    # Using other user's auth token
    auth = {'X-Authorization': TEST_TOKEN2}
    get(client.fc, SESSION_URL, 403, auth=auth)

def test_incorrect_create_session(client: Client):
    print(f'\nTesting incorrect POST for {SESSION_URL} ', end='')
    # Incorrect authorization
    auth = {'X-Authorization': 'TEST_TOKEN'}
    json = {'name': 'epic session'}
    post(client.fc, SESSION_URL, json, 403, auth=auth)

    # Missing authorization
    post(client.fc, SESSION_URL, json, 403, auth=None)

    # Using other user's auth token
    auth = {'X-Authorization': TEST_TOKEN2}
    post(client.fc, SESSION_URL, json, 403, auth=auth)

    # Incorrect JSON
    json['name'] = ''
    post(client.fc, SESSION_URL, json, 409)
    post(client.fc, SESSION_URL, {'a': 'b'}, 400, json_fail=True) # Doesn't match schema
    post(client.fc, SESSION_URL, None, 415, json_fail=True) # Incorrect media type

    # Session already exists
    json['name'] = TEST_SESSION
    post(client.fc, SESSION_URL, json, 409)

def test_get_session_item(client: Client):
    print(f'\nTesting GET for {SESSION_ITEM_URL} ', end='')
    get(client.fc, SESSION_ITEM_URL, 200)

def test_incorrect_get_session_item(client: Client):
    print(f'\nTesting incorrect GET for {SESSION_ITEM_URL} ', end='')
    # Incorrect authorization
    auth = {'X-Authorization': 'TEST_TOKEN'}
    get(client.fc, SESSION_ITEM_URL, 403, auth=auth)

    # No authorization
    get(client.fc, SESSION_ITEM_URL, 403, auth=None)

    # Using other user's auth token
    auth = {'X-Authorization': TEST_TOKEN2}
    get(client.fc, SESSION_ITEM_URL, 403, auth=auth)

    # Session doesn't exist
    get(client.fc, SESSION_URL+'session_404/', 404)

def test_put_accessed_file(client: Client):
    print(f'\nTesting PUT for {ACCESSED_FILE_URL} ', end='')
    put(client.fc, ACCESSED_FILE_URL, ACCESSED_FILE_JSON, 201)
    # Make sure that 3 accessed files were added to the database
    assert AccessedFile.query.count() == 3

def test_incorrect_put_accessed_file(client: Client):
    print(f'\nTesting incorrect PUT for {ACCESSED_FILE_URL} ', end='')
    # JSON schema related
    put(client.fc, ACCESSED_FILE_URL, [{}], 400, json_fail=True)
    json = [{'id': 1, 'filepath': 'a/a/a'}]
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)
    json = copy.deepcopy(ACCESSED_FILE_JSON)
    json[1]['id'] = 'a'
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)
    json = copy.deepcopy(ACCESSED_FILE_JSON)
    json[1]['created_at'] = 10.4
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)
    json = copy.deepcopy(ACCESSED_FILE_JSON)
    json[1]['pipe'] = 'a'
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)
    json = copy.deepcopy(ACCESSED_FILE_JSON)
    json[1]['inode'] = 10.4
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)
    json = copy.deepcopy(ACCESSED_FILE_JSON)
    json[1]['extra_val'] = 'test'
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)
    
    # Incorrect datetimes
    json = copy.deepcopy(ACCESSED_FILE_JSON)
    json[1]['created_at'] = '2021-06-16 24:29:26'
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=False)
    json[1]['created_at'] = '2021-13-16 23:29:26'
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=False)
    json[1]['created_at'] = '2021-06-16 23:29:2a'
    put(client.fc, ACCESSED_FILE_URL, json, 400, json_fail=True)

    # Incorrect authorization
    auth = {'X-Authorization': 'TEST_TOKEN'}
    put(client.fc, ACCESSED_FILE_URL, ACCESSED_FILE_JSON, 403, auth=auth)

    # No authorization
    put(client.fc, ACCESSED_FILE_URL, ACCESSED_FILE_JSON, 403, auth=None)

    # Using other user's auth token
    auth = {'X-Authorization': TEST_TOKEN2}
    put(client.fc, ACCESSED_FILE_URL, ACCESSED_FILE_JSON, 403, auth=auth)

    # Non existent session
    put(client.fc, SESSION_URL+'missing/accessed-files/', ACCESSED_FILE_JSON, 404)

def test_put_process(client: Client):
    print(f'\nTesting PUT for {ACCESSED_FILE_URL} ', end='')
    put(client.fc, PROCESS_URL, PROCESS_JSON, 201)
    # Make sure that 4 processes were added to the database
    assert Process.query.count() == 4
    # Check parent process relations
    process1 = Process.query.filter_by(id=1).first()
    process2 = Process.query.filter_by(id=2).first()
    assert process2.parent == process1

def test_incorrect_put_process(client: Client):
    print(f'\nTesting incorrect PUT for {PROCESS_URL} ', end='')
    # JSON schema related
    put(client.fc, PROCESS_URL, [{}], 400, json_fail=True)
    json = [{'id': 1, 'filepath': 'a/a/a'}]
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)
    json = copy.deepcopy(PROCESS_JSON)
    json[1]['id'] = 'a'
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)
    json = copy.deepcopy(PROCESS_JSON)
    json[1]['pid'] = 'a'
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)
    json = copy.deepcopy(PROCESS_JSON)
    json[1]['parent_id'] = -1
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)
    json = copy.deepcopy(PROCESS_JSON)
    json[1]['created_at'] = 10.4
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)
    json = copy.deepcopy(PROCESS_JSON)
    json[1]['extra_val'] = 'test'
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)

    # Incorrect parent ID
    json = copy.deepcopy(PROCESS_JSON)
    json[0]['parent_id'] = 1
    put(client.fc, PROCESS_URL, json, 400, json_fail=False)

    # Incorrect datetimes
    json = copy.deepcopy(PROCESS_JSON)
    json[1]['created_at'] = '2021-06-16 24:29:26'
    put(client.fc, PROCESS_URL, json, 400, json_fail=False)
    json[1]['created_at'] = '2021-13-16 23:29:26'
    put(client.fc, PROCESS_URL, json, 400, json_fail=False)
    json[1]['created_at'] = '2021-06-16 23:29:2a'
    put(client.fc, PROCESS_URL, json, 400, json_fail=True)

    # Incorrect authorization
    auth = {'X-Authorization': 'TEST_TOKEN'}
    put(client.fc, PROCESS_URL, PROCESS_JSON, 403, auth=auth)

    # No authorization
    put(client.fc, PROCESS_URL, PROCESS_JSON, 403, auth=None)

    # Using other user's auth token
    auth = {'X-Authorization': TEST_TOKEN2}
    put(client.fc, PROCESS_URL, PROCESS_JSON, 403, auth=auth)

    # Non existent session
    put(client.fc, SESSION_URL+'missing/processes/', PROCESS_JSON, 404)

def test_put_syscall(client: Client):
    print(f'\nTesting PUT for {SYSCALL_URL} ', end='')
    # For relation testing
    put(client.fc, ACCESSED_FILE_URL, ACCESSED_FILE_JSON, 201)
    put(client.fc, PROCESS_URL, PROCESS_JSON, 201)

    put(client.fc, SYSCALL_URL, SYSCALL_JSON, 201)
    # Make sure that 3 syscalls were added to the database
    assert Syscall.query.count() == 3
    # Check relations to AccessedFile and Process
    syscall1 = Syscall.query.filter_by(id=1).first()
    assert syscall1.file_id is None
    assert syscall1.process_id is None
    syscall2 = Syscall.query.filter_by(id=2).first()
    assert syscall2.file_id == 2
    assert syscall2.process_id == 1

def test_incorrect_put_syscall(client: Client):
    print(f'\nTesting incorrect PUT for {SYSCALL_URL} ', end='')
    # JSON schema related
    put(client.fc, SYSCALL_URL, [{}], 400, json_fail=True)
    json = copy.deepcopy(SYSCALL_JSON)
    json[0]['name'] = 'WROTE'
    put(client.fc, SYSCALL_URL, json, 400, json_fail=True)
    json = copy.deepcopy(SYSCALL_JSON)
    json[0]['file_id'] = -1
    put(client.fc, SYSCALL_URL, json, 400, json_fail=True)
    json = copy.deepcopy(SYSCALL_JSON)
    json[0]['process_id'] = 'WROTE'
    put(client.fc, SYSCALL_URL, json, 400, json_fail=True)
    json = copy.deepcopy(SYSCALL_JSON)
    json[0]['arg0'] = True
    put(client.fc, SYSCALL_URL, json, 400, json_fail=True)
    json = copy.deepcopy(SYSCALL_JSON)
    json[0]['extra_val'] = 'test.txt'
    put(client.fc, SYSCALL_URL, json, 400, json_fail=True)

    # Incorrect datetimes
    json = copy.deepcopy(SYSCALL_JSON)
    json[1]['created_at'] = '2021-06-16 24:29:26'
    put(client.fc, SYSCALL_URL, json, 400, json_fail=False)
    json[1]['created_at'] = '2021-13-16 23:29:26'
    put(client.fc, SYSCALL_URL, json, 400, json_fail=False)
    json[1]['created_at'] = '2021-06-16 23:29:2a'
    put(client.fc, SYSCALL_URL, json, 400, json_fail=True)
    
    # Incorrect authorization
    auth = {'X-Authorization': 'TEST_TOKEN'}
    put(client.fc, SYSCALL_URL, SYSCALL_JSON, 403, auth=auth)

    # No authorization
    put(client.fc, SYSCALL_URL, SYSCALL_JSON, 403, auth=None)

    # Using other user's auth token
    auth = {'X-Authorization': TEST_TOKEN2}
    put(client.fc, SYSCALL_URL, SYSCALL_JSON, 403, auth=auth)

    # Non existent session
    put(client.fc, SESSION_URL+'missing/syscalls/', SYSCALL_JSON, 404)
