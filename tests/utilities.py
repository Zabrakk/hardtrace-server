import pytest
from flask.testing import FlaskClient
from flask_sqlalchemy import SQLAlchemy
from jsonschema import validate, ValidationError

from tests.constants import *

class Client:
    """
    Holds the FlaskClient and access to the database.
    Passed to test functions
    """
    def __init__(self, fc: FlaskClient, db: SQLAlchemy) -> None:
        self.fc = fc
        self.db = db


def get(client: FlaskClient, url: str, status_code: int, schema: dict=None, json: dict=None, auth: dict= {'X-Authorization': TEST_TOKEN}):
    """
    Sends a test GET request to the given URL.
    Compares the provided status code and (optional) JSON to the response
    """
    # Status code test
    r = client.get(url, headers=auth)
    assert r.status_code == status_code
    # Schema test
    if schema is not None:
        try:
            assert validate(r.json, schema) == None
        except ValidationError:
            pytest.fail('Validation error')
    # JSON test
    if json is not None:
        assert r.json == json

def post(client: FlaskClient, url: str, json: dict, status_code: int, auth = {'X-Authorization': TEST_TOKEN}, json_fail: bool=False):
    r = client.post(url, json=json, headers=auth)
    if 'invalid json' in str(r.data).lower() and not json_fail:
        pytest.fail('Invalid JSON format')
    else:
        pass
    assert r.status_code == status_code

def put(client: FlaskClient, url: str, json: dict, status_code: int, auth = {'X-Authorization': TEST_TOKEN}, json_fail: bool=False):
    r = client.put(url, json=json, headers=auth)
    if 'invalid json' in str(r.data).lower() and not json_fail:
        pytest.fail('Invalid JSON format')
    else:
        pass
    assert r.status_code == status_code
