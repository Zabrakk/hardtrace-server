import os
import logging
from server.models.user import User

logger = logging.getLogger('Authorization')

class Auth:
    """
    Authorization checks for the upload/ and analysis/ API resources.
    Checks that a specific username and token are provided in requests.
    Username and token read from the VISUALIZER_USER and VISUALIZER_TOKEN environment variables
    """
    def __init__(self) -> None:
        self.visualizer_user = os.getenv('VISUALIZER_USER')
        self.visualizer_token = os.getenv('VISUALIZER_TOKEN')

    def auth_hardtrace(self, user: User, token: str) -> bool:
        """
        Authorizes data upload requests sent by Hardtrace based on the provided user pseudonym and token.
        The usernames and tokens are created by the CreateUser API resource.
        """
        if user is None or token is None:
            logger.info('User or authentication token missing from request')
            return False
        if user.auth_token != token:
            logger.info('User and authentication token do not match')
            return False
        return True

    def auth_visualizer(self, user: str, token: str) -> bool:
        """
        Authorizes requests sent to the analysis/ resources based on the provided user pseudonym and token
        """
        if user == self.visualizer_user and token == self.visualizer_token:
            return True
        logger.info("Incorrect authentication details provided for visualizer")
        return False

# Initialize the Auth object
auth = Auth()
