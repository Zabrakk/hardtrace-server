from flask import Blueprint
from flask_restful import Api
# API resource imports
from server.resources.status import Status
from server.resources.create_user import CreateUser
from server.resources.analysis.user_info import UserInfo
from server.resources.analysis.file_info import FileInfo
from server.resources.analysis.summary import SummaryResource
# Different data types
from server.resources.upload.accessed_files import AccessedFileCollection
from server.resources.upload.processes import ProcessCollection
from server.resources.upload.syscalls import SyscallCollection
from server.resources.upload.session import SessionCollection, SessionItem

blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(blueprint)

# API resources and their paths:
# Utilities
api.add_resource(Status, '/status/') # The /api prefix is automatically added infornt of all of these
api.add_resource(CreateUser, '/create-user/')
api.add_resource(UserInfo, '/analysis/user-info/')
api.add_resource(FileInfo, '/analysis/file-info/')
api.add_resource(SummaryResource, '/analysis/summary/')
# Data uploads
api.add_resource(SessionCollection, '/users/<user>/sessions/')
api.add_resource(SessionItem, '/users/<user>/sessions/<session_name>/')
api.add_resource(SyscallCollection, '/users/<user>/sessions/<session_name>/syscalls/')
api.add_resource(ProcessCollection, '/users/<user>/sessions/<session_name>/processes/')
api.add_resource(AccessedFileCollection, '/users/<user>/sessions/<session_name>/accessed-files/')
