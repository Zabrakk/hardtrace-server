from server import db
from server.utilities import datetime_to_str


class AccessedFile(db.Model):
    """
    Python adaptation of the AccessedFiles in Hardtrace's database
    """
    id = db.Column(db.Integer, primary_key=True)
    session_id = db.Column(db.ForeignKey("session.id", ondelete="CASCADE", onupdate="CASCADE"), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    hardtrace_id = db.Column(db.Integer, nullable=False)
    filepath = db.Column(db.String, nullable=False)
    pipe = db.Column(db.Boolean, nullable=False)
    inode = db.Column(db.Integer, nullable=False)
    hash = db.Column(db.String, nullable=False)

    session = db.relationship("Session", back_populates="accessed_files")


    @staticmethod
    def schema() -> dict:
        """
        JSON schema for upload validation
        """
        return {
            'type': 'array',
            'maxProperties': 1,
            'items': {
                # Not including deleted_at, since it won't be used
                'required': ['id', 'created_at', 'filepath', 'pipe', 'inode' , 'hash'],
                'minProperties': 6,
                'maxProperties': 6,
                'properties': {
                    'id': {
                        'type': 'integer',
                        'minimum': 0
                    },
                    'created_at': {
                        'type': 'string',
                        'pattern': "^[0-9]{4}-[01][0-9]-[0-3][0-9] [0-9][0-9]:[0-5][0-9]:[0-5][0-9]$"
                    },
                    'filepath': {
                        'type': 'string'
                    },
                    'pipe': {
                        'type': 'boolean'
                    },
                    'inode': {
                        'type': 'integer'
                    },
                    'hash': {
                        'type': 'string'
                    }
                }
            }
        }

    def to_json(self) -> dict:
        """
        Returns the JSON representation of an AccessedFile
        """
        return {
            'created_at': datetime_to_str(self.created_at),
            'filepath': self.filepath.rsplit('/', 1)[1] if '/' in self.filepath else self.filepath, # Leave out the path if it is included
            'pipe': self.pipe,
            'inode': self.inode,
            'hash': self.hash
        }

    def __repr__(self) -> str:
        """
        Returns the string representation of an AccessedFile
        """
        return f'{self.filepath}, {self.hash}'
