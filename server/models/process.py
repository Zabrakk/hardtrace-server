from server import db
from server.utilities import datetime_to_str

class Process(db.Model):
    """
    Python adaptation of Processes in Hardtrace's database
    """
    id = db.Column(db.Integer, primary_key=True)
    session_id = db.Column(db.ForeignKey("session.id", ondelete="CASCADE", onupdate="CASCADE"), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    hardtrace_id = db.Column(db.Integer, nullable=False)
    pid = db.Column(db.Integer, nullable=False)
    parent_id = db.Column(db.ForeignKey("process.id"), nullable=True)

    session = db.relationship("Session", back_populates="processes")
    # Reference to the same table
    child = db.relationship("Process", backref=db.backref("parent", remote_side=[id]))

    @staticmethod
    def schema() -> dict:
        """
        JSON schema for upload validation
        """
        return {
            'type': 'array',
            'items': {
                # Not including deleted_at, since it won't be used
                'required': ['id', 'created_at', 'pid', 'parent_id'],
                'minProperties': 4,
                'maxProperties': 4,
                'properties': {
                    'id': {
                        'type': 'integer',
                        'minimum': 0
                    },
                    'created_at': {
                        'type': 'string',
                        'pattern': "^[0-9]{4}-[01][0-9]-[0-3][0-9] [0-9][0-9]:[0-5][0-9]:[0-5][0-9]$"
                    },
                    'pid': {
                        'type': 'integer'
                    },
                    'parent_id': {
                        'type': 'integer',
                        'minimum': 0
                    }
                }
            }
        }

    def to_json(self) -> dict:
        """
        Returns the JSON representation of a Process
        """
        return {
            'created_at': datetime_to_str(self.created_at),
            'pid': self.pid,
            'parent_id': self.parent_id if self.parent_id is not None else 0
        }
