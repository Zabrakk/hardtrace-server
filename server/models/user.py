from server import db


class User(db.Model):
    """
    Database model for Hardtrace users
    """
    id = db.Column(db.Integer, primary_key=True)
    pseudonym = db.Column(db.String, unique=True)
    auth_token = db.Column(db.String, unique=True, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)

    sessions = db.relationship("Session", cascade="all, delete-orphan", back_populates="user")

    def to_json(self):
        """
        Used to provide the visualization application with a JSON containing users' pseudonyms and session names.
        Formatting:
        {
            "pseudonym": ["session1", "session2", ...]
        }
        """
        return {
            self.pseudonym: [session.name for session in self.sessions]
        }

    def __repr__(self):
        """
        Returns the string representation of a User
        """
        return f'ID: {self.id}, Token: {self.auth_token}, Created at: {self.created_at}'
