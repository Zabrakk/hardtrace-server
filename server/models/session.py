from server import db
from server.utilities import datetime_to_str

class Session(db.Model):
    """
    Model for session database objects
    """
    # Ensure that a user can't have two sessions with the same name in the database
    __table_args__ = (db.UniqueConstraint("user_id", "name", name="_session_to_user_unique_constraint"),)

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String)

    # Connecting sessins to other models
    user = db.relationship("User", back_populates="sessions")
    accessed_files = db.relationship("AccessedFile", cascade="all, delete-orphan", back_populates="session")
    syscalls = db.relationship("Syscall", cascade="all, delete-orphan", back_populates="session")
    processes = db.relationship("Process", cascade="all, delete-orphan", back_populates="session")


    def __repr__(self) -> str:
        return f'ID: {self.id}, Name: {self.name}, Created at: {self.created_at}'

    @staticmethod
    def schema() -> dict:
        """
        JSON schema for upload validation
        """
        return {
            'type': 'object',
            'required': ['name'],
            'properties': {
                'name': {
                    'type': 'string'
                }
                # Created_at & user is set by the API
            }
        }

    def to_json(self) -> dict:
        """
        Returns the JSON representation of a Session
        """
        return {
                'pseudonym': self.user.pseudonym,
                'created_at': datetime_to_str(self.created_at),
                'name': self.name,
                'accessed_files': [file.to_json() for file in self.accessed_files],
                'processes': [process.to_json() for process in self.processes],
                'syscalls': [syscall.to_json() for syscall in self.syscalls]
        }
