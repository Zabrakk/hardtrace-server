from server import db
from server.utilities import datetime_to_str


class Syscall(db.Model):
    """
    Python adaptation of Syscalls in Hardtrace's database
    """
    id = db.Column(db.Integer, primary_key=True)
    session_id = db.Column(db.ForeignKey('session.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String, nullable=False)
    process_id = db.Column(db.Integer, nullable=True)
    file_id = db.Column(db.Integer, nullable=True)
    arg0 = db.Column(db.String, nullable=False)
    args = db.Column(db.String, nullable=True)

    session = db.relationship('Session', back_populates='syscalls')

    @staticmethod
    def schema() -> dict:
        """
        JSON schema for upload validation
        """
        return {
            'type': 'array',
            'items': {
                'required': ['created_at', 'name', 'process_id', 'file_id' , 'arg0'],
                'minProperties': 5,
                'maxProperties': 6,
                'properties': {
                    'created_at': {
                        'type': 'string',
                        'pattern': '^[0-9]{4}-[01][0-9]-[0-3][0-9] [0-9][0-9]:[0-5][0-9]:[0-5][0-9]$'
                    },
                    'name': {
                        'type': 'string',
                        'enum': ['READ', 'WRITE', 'OPENAT', 'RENAME', 'CHDIR', 'CLOSE', 'EXECVE', 'PIPE']
                    },
                    'process_id': {
                        'type': 'integer',
                        'minimum': 0
                    },
                    'file_id': {
                        'type': 'integer',
                        'minimum': 0
                    },
                    'arg0': {
                        'type': 'string'
                    },
                    'args': {
                        'type': 'string'
                    }
                }
            }
        }

    def to_json(self) -> dict:
        '''
        Returns the JSON representation of a Syscall
        '''
        return {
            'created_at': datetime_to_str(self.created_at),
            'name': self.name,
            'process_id': self.process_id if self.process_id is not None else 0,
            'file_id': self.file_id if self.file_id is not None else 0,
            'arg0': self.arg0,
            'args': self.args
        }

    def __repr__(self) -> str:
        """
        Returns the string representation of a Syscall
        """
        return f'Syscall: pid:{self.process_id}, arg0: {self.arg0}, args: {self.args}'
