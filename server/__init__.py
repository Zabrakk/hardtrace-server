import os
import logging
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import event
from sqlalchemy.engine import Engine

# Create the database object
db = SQLAlchemy()

# Imports requiring db to exist
from . import constants
from . import api
from .cmd import init_db_cmd

# Set up the logger
logging.basicConfig(filename='api.log', filemode='w', level=logging.INFO, datefmt='%H:%M:%S',
    format='%(asctime)s %(name)s: %(message)s')

def create_app(config=None):
    """
    Based on the example found here: https://flask.palletsprojects.com/en/2.0.x/tutorial/factory/
    """
    logger = logging.getLogger('init')

    # Create the Flask app
    logger.info('Creating Flask application')
    app = Flask(__name__, instance_relative_config=True)

    # Handle Cross Origin Resource Sharing
    CORS(app)

    # Database configuration
    if config is None:
        if os.getenv("USE_LOCAL_DB") == "true":
            logger.info(f'Using database at ../{constants.DB_PATH}')
            app.config.from_mapping(
                # Use a local SQLite database
                SQLALCHEMY_DATABASE_URI = f'sqlite:///../{constants.DB_PATH}',
                SQLALCHEMY_TRACK_MODIFICATIONS=False
            )
        else:
            logger.info('Connecting to PostgreSQL database')
            app.config.from_mapping(
                # Connect to a PostgreSQL database
                SQLALCHEMY_DATABASE_URI = f'postgresql://{os.getenv("DB_USER")}:{os.getenv("DB_PASS")}@{os.getenv("DB_URL")}/{os.getenv("DB_NAME")}',
                SQLALCHEMY_TRACK_MODIFICATIONS=False
            )
    else:
        logger.info('Using custom database config')
        app.config.from_mapping(config)

    # Add database to Flask app
    db.init_app(app)
    @event.listens_for(Engine, "connect")
    def set_sqlite_pragma(dbapi_connection, connection_record):
        cursor = dbapi_connection.cursor()
        cursor.close()

    # $ flask init
    # Initializes the database. If the database already contains something, all contents will be removed
    app.cli.add_command(init_db_cmd)

    # Register API blueprint to the app
    app.register_blueprint(api.blueprint)

    logger.info('Flask app created')
    return app
