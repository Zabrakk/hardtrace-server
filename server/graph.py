import logging
from typing import List

from server.utilities import datetime_to_str
from server.models.accessed_file import AccessedFile
from server.summary import SessionSummary, FileSummary, ProcessSummary

logger = logging.getLogger('Network')


class Node:
    """
    Graph nodes, representing the files encountered during Hardtrace sessions
    """
    def __init__(self, id: int, name: str, created_at: str, hash: str, inode: int) -> None:
        self.id = id
        self.name = name                # Name of the file
        self.created_at = created_at    # When the file was created
        self.hash = hash                # File's hash
        self.inode = inode
        self.source_node = True
        self.selected_file = False

    def to_json(self) -> dict:
        """
        Returns the node's JSON represnetation
        """
        return {
            'id': self.id,
            'name': self.name,
            'created_at': self.created_at,
            'hash': self.hash,
            'inode': self.inode,
            'source_node': self.source_node,
            'selected_file': self.selected_file
        }


class Edge:
    """
    Graph edges representing the commands used during Hardtrace sessions.
    """
    def __init__(self, s: int or None, t: int or None, short_cmd: str, cmd: str) -> None:
        self.source = s                 # Id of the node from which the edge starts
        self.target = t                 # Id of the node to where the edge ends
        self.short_cmd = short_cmd      # Start of the command, e.g., "nano"
        self.cmd = cmd                  # Full command, e.g., "nano file.txt"
        self.run_by = []                # type: List[str] # Hardtrace users who used this commands

    def add_user(self, pseudonym: str) -> None:
        """
        Adds a user's pseudonym to the edge.
        This shows who used which commands
        """
        if pseudonym not in self.run_by:
            self.run_by.append(pseudonym)

    def append_cmd(self, cmd: str, pseudonym) -> None:
        """
        Used to combine differing commands that had the same source and target file
        """
        if pseudonym in self.run_by:
            return
        if cmd != self.cmd:
            self.cmd += f'<$%&>{cmd}'

    def _update_cmd(self) -> str:
        """
        Adds user pseudonym to edges where multiple different commands are stored
        """
        split_cmd = self.cmd.split('<$%&>')
        if len(split_cmd) == 1:
            return
        self.cmd = ""
        for i in range(len(split_cmd)):
            split_cmd[i] = f'{self.run_by[i]}: {split_cmd[i]}'
        self.cmd = '<$%&>'.join(split_cmd)

    def to_json(self) -> dict:
        """
        Returns the edge's JSON representation
        """
        self._update_cmd()
        return {
            'cmd': self.cmd,
            'short_cmd': self.short_cmd,
            'source': self.source,
            'target': self.target,
            'run_by': " & ".join(self.run_by),
        }


class Graph:
    """
    Used to turn SessionSummaries into graph data
    """
    def __init__(self, summaries: List[SessionSummary] or List[FileSummary]) -> None:
        self.summaries = summaries
        self.nodes = []             # type: List[Node]
        self.edges = []             # type: List[Edge]

    def create(self) -> None:
        """
        Starts the graph data creation
        """
        s: SessionSummary
        # Iterate through all given summaries
        for s in self.summaries:
            logger.info(f"Creating graph data for {s.pseudonym}'s session {s.name}")
            ps: ProcessSummary
            # Extract edges and nodes from individual ProcessSummaries
            for ps in s.p_sums:
                self._add_nodes(ps)
                self._add_edges(ps, s.pseudonym)
        self._add_empty_nodes()
        logger.info('Graph created')

    def to_json(self) -> dict:
        """
        Returns the JSON representation of the Graph
        """
        return {
            'nodes': [node.to_json() for node in self.nodes],
            'edges': [edge.to_json() for edge in self.edges]
        }

    def _add_nodes(self, ps: ProcessSummary) -> None:
        """
        Extracts files from the given ProcessSummary and adds them as nodes to the graph
        """
        f: AccessedFile
        for f in ps.old_files + ps.new_files:
            if self._should_add_node(f):
                self._create_node(f)

    def _should_add_node(self, f: AccessedFile) -> bool:
        """
        Determines if a file should be added as a node to the network.
        Returns True if a file with the same hash has not been previously added
        """
        return f.hash not in [node.hash for node in self.nodes]

    def _create_node(self, f: AccessedFile) -> Node:
        """
        Creates and appends a node object to the Graph based on the given AccessedFile
        """
        node = Node(
            id=len(self.nodes),
            name=f.filepath,
            created_at=datetime_to_str(f.created_at),
            hash=f.hash,
            inode=f.inode
        )
        self.nodes.append(node)

    def _get_node_id(self, f: AccessedFile) -> int or None:
        """
        Returns None or the id of the node the given AccessedFile represents
        """
        for node in self.nodes:
            if f.hash == node.hash:
                return node.id
        return None

    def _add_empty_nodes(self) -> None:
        """
        Connects edges to empty source or target nodes for visualization purposes
        """
        for edge in self.edges:
            if edge.source is None:
                edge.source = self._create_empty_node()
            elif edge.target is None:
                edge.target = self._create_empty_node()

    def _create_empty_node(self) -> int:
        """
        Adds an empty node to the graph.
        Returns the created node's id
        """
        id = len(self.nodes)
        self.nodes.append(Node(
            id=id,
            name=None,
            created_at=None,
            hash=None,
            inode=None
        ))
        return id

    def _add_edges(self, ps: ProcessSummary, pseudonym: str) -> None:
        """
        Extracts commands from given ProcessSummaries and adds them as edges to the graph with _add_edge()
        """
        source_ids = [self._get_node_id(f) for f in ps.old_files]
        target_ids = [self._get_node_id(f) for f in ps.new_files]
        if len(source_ids) == 0:
            for t_id in target_ids:
                self._create_edge(None, t_id, ps, pseudonym)
        elif len(target_ids) == 0:
            for s_id in source_ids:
                self._create_edge(s_id, None, ps, pseudonym)
        else:
            for s_id in source_ids:
                for t_id in target_ids:
                    self._create_edge(s_id, t_id, ps, pseudonym)

    def _get_edge(self, s_id: int or None, t_id: int or None, cmd: str, short_cmd: str) -> Edge or None:
        """
        Returns None or the edge represented by the given parameters
        """
        for edge in self.edges:
            if edge.source == s_id and edge.target == t_id and (edge.cmd == cmd or edge.short_cmd == short_cmd):
                return edge
        return None

    def _create_edge(self, s_id: int, t_id: int, ps: ProcessSummary, pseudonym: str) -> None:
        """
        Creates and and appends an edge object to the Graph based on the given parameters.
        If an edge that represents very similar actions already exists, the new edge is combined into the existing one
        """
        if s_id is None and ps.cmd_type == "READ":
            s_id = t_id
            t_id = None
        edge = self._get_edge(s_id, t_id, ps.cmd, ps.cmd_short)
        if edge is not None:
            # Update / combine to an already existing edge representing very similar actions
            edge.append_cmd(ps.cmd, pseudonym)
            edge.add_user(pseudonym)
        else:
            # Create a new edge
            edge = Edge(
                s=s_id,
                t=t_id,
                short_cmd=ps.cmd_short,
                cmd=ps.cmd
            )
            edge.add_user(pseudonym)
            self.edges.append(edge)
            if s_id is not None and t_id is not None:
                self.nodes[t_id].source_node = False

    def _mark_nodes_not_source(self) -> None:
        """
        Mark nodes that are not source source nodes.
        Used for visualization purposes
        """
        for edge in self.edges:
            if edge.target is not None:
                self.nodes[edge.target].source_node = False

    def mark_files_as_selected(self, files: List[AccessedFile]) -> None:
        """
        Marks the specified files so they can be displayed with a different color in the visualization.
        Used with FileSummary
        """
        for f in files:
            for node in self.nodes:
                if node.name == f.filepath and node.hash == f.hash:
                    node.selected_file = True
