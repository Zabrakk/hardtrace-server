import logging
from typing import List
from datetime import datetime

from server.utilities import datetime_to_str
from server.models.session import Session
from server.models.syscall import Syscall
from server.models.accessed_file import AccessedFile

logger = logging.getLogger('SessionSummary')


class ProcessSummary:
    """
    Represents summaries created for processes found in Hardtrace data
    """
    def __init__(self, pid: int, run_at: datetime) -> None:
        self.pid = pid
        self.cmd = ''           # Command that was run by the Hardtrace user, e.g., "nano test.txt"
        self.cmd_short = ''     # Start of the command, e.g., "nano"
        self.cmd_type = ''      # READ or WRITE
        self.run_at = run_at    # Approximate time on when the process started
        self.run_by = ''        # Pseudonyms of users who had very similar processes
        self.old_files = []     # type: List[AccessedFile]
        self.new_files = []     # type: List[AccessedFile]

    def to_json(self) -> dict:
        """
        Returns the JSON representation of a ProcessSummary
        """
        return {
            'cmd': self.cmd,
            'cmd_type': self.cmd_type,
            'run_at': datetime_to_str(self.run_at),
            'old_files': [f.to_json() for f in self.old_files],
            'new_files': [f.to_json() for f in self.new_files]
        }

    def __repr__(self) -> str:
        """
        Returns the string representation of a ProcessSummary
        """
        return f'ProcSum - PID: {self.pid}, cmd: {self.cmd}, Old: {len(self.old_files)}, New: {len(self.new_files)}'


class SessionSummary:
    """
    Represents summarized Hardtrace sessions.
    """
    def __init__(self, pseudonym: str, name: str, session: Session, ignore_files: List[str]) -> None:
        self.pseudonym = pseudonym  # Hardtrace user's pseudonym
        self.name = name            # Name of the session
        self.session = session
        self.i_f = ignore_files     # Files that should be ignored when creating the session
        # identifiers includes the hash and inode of a file
        self.identifiers = {}       # type: dict[str, int]
        self.p_sums = []            # type: List[ProcessSummary]

    def create(self) -> None:
        """
        Starts the summary creation process
        """
        logger.info("Summarizing session user %s's session %s", self.pseudonym, self.name)
        syscall = None              # type: Syscall
        for syscall in self.session.syscalls:
            pid = syscall.process_id
            f = AccessedFile.query.filter_by(session=self.session, hardtrace_id=syscall.file_id).first()

            # Grouping syscalls based on PID
            if pid not in self._get_pids():
                ps = ProcessSummary(
                    pid=pid,
                    run_at=syscall.created_at
                )
                self.p_sums.append(ps)

            ps = self._get_proc_sum(pid)
            # The commands run by users can be found in EXECVE syscalls
            if syscall.name == 'EXECVE':
                ps.cmd = syscall.arg0
                if len(syscall.args) > 0:
                    ps.cmd += " " + syscall.args.replace("\n", " ")
                ps.cmd_short = ps.cmd.split(' ')[0]

            if ps.cmd_type == "" and syscall.name == "READ":
                ps.cmd_type = "READ"
            if syscall.name == "WRITE":
                ps.cmd_type = "WRITE"

            # Add accessed files to current ProcessSummary
            if self._should_add_file(f):
                if self._should_add_new_file(f):
                    self.identifiers[f.hash] = f.inode
                    ps.new_files.append(f)
                elif self._should_add_old_file(f, ps):
                    ps.old_files.append(f)

        self.remove_processes_with_no_cmd()
        logger.info('Summary created')


    def _get_pids(self) -> List[int]:
        """
        Returns the PIDs of each process summary
        """
        return [p.pid for p in self.p_sums]

    def _get_proc_sum(self, pid: int) -> ProcessSummary:
        """
        Returns the ProcessSummary matching the given PID
        """
        return [p for p in self.p_sums if p.pid == pid][0]

    def _should_add_file(self, f: AccessedFile) -> bool:
        """
        Determine if a given file is valid and could be added to the ProcessSummary.
        Exludes files that are None, ignored, pipes, hidden or empty.
        """
        if f is None or f.filepath in self.i_f or f.hash in self.i_f:
            return False
        if f.pipe or f.filepath.startswith(".") or f.hash == 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855':
            return False
        if len(self.i_f) > 0:
            # File ignore rule check.
            # If the rule "test*" is used, all files with names starting with "test" will be ignored.
            # Works similarly for "*test"
            for i_f in self.i_f:
                if i_f.startswith("*") and f.filepath.endswith(i_f[1:]) or i_f.endswith("*") and f.filepath.startswith(i_f[:-1]):
                    return False
        return True

    def _should_add_new_file(self, f: AccessedFile) -> bool:
        """
        Determine if the given file should be added to new files.
        To be added to new files, the file's hash and inode should not have appeared previosuly during the session
        """
        return f.hash not in self.identifiers.keys()

    def _should_add_old_file(self, f: AccessedFile, ps: ProcessSummary) -> bool:
        """
        Determines if the given file should be added as an old file.
        Old files have appeared previously during the session
        """
        if f.hash in [x.hash for x in ps.old_files + ps.new_files] or f.inode in [x.hash for x in ps.old_files + ps.new_files]:
            return False
        return f.hash in self.identifiers.keys() and f.inode == self.identifiers[f.hash]

    def remove_processes_with_no_cmd(self) -> None:
        """
        Removes ProcessSummaries where the command is empty
        """
        self.p_sums = [ps for ps in self.p_sums if len(ps.cmd) > 0]

    def to_json(self) -> dict:
        """
        Returns the JSON representation of SessionSummary
        """
        return {
            'pseudonym': self.pseudonym,
            'session': self.name,
            'process_summary': [ps.to_json() for ps in self.p_sums]
        }

    def remove_processes_with_no_files(self) -> None:
        """
        Removes ProcessSummaries that did not modify any files
        """
        self.p_sums = [ps for ps in self.p_sums if len(ps.old_files + ps.new_files) > 0]

    def remove_unrelated_files(self, files: List[AccessedFile]) -> None:
        """
        Removes ProcessSummaries that are not at all related to the given list of AccessedFiles
        """
        ids = [[f.hash, f.inode] for f in files]
        dont_remove = []  # type: List[ProcessSummary]
        self.p_sums = reversed(self.p_sums)
        for ps in self.p_sums:
            if ps.cmd_type == "READ" and len(ps.old_files) == 0:
                # Stop these read commands from messing up the file network
                continue
            for f in ps.new_files:
                if [f.hash, f.inode] in ids:
                    dont_remove.append(ps)
                    ids += [[o_f.hash, o_f.inode] for o_f in ps.old_files]
        for ps in dont_remove:
            ps.new_files = [n_f for n_f in ps.new_files if [n_f.hash, n_f.inode] in ids]
        self.p_sums = reversed(dont_remove)


class FileSummary:
    """
    Represents Hardtrace sessions created to only show the path leading to the creation of specific AccessedFiles
    """
    def __init__(self, files: List[AccessedFile], sessions: List[Session], ignore_files: List[str]) -> None:
        self.files = files
        self.sessions = sessions
        self.summaries = []         # type: List[SessionSummary]
        self.i_f = ignore_files

    def create(self) -> None:
        """
        Creates a summary of the given sessions that only includes processes and files
        directly related to the user provided list of specific files
        """
        for session in self.sessions:
            summary = SessionSummary(session.user.pseudonym, session.name, session, self.i_f)
            summary.create()
            summary.remove_processes_with_no_files()
            summary.remove_unrelated_files(self.files)

            self.summaries.append(summary)

    def to_json(self) -> dict:
        """
        Returns the JSON representation of FileSummary
        """
        return [summary.to_json() for summary in self.summaries]
