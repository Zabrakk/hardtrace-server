import logging
import datetime
from typing import Union, List

from jsonschema import validate
from jsonschema.exceptions import ValidationError

logger = logging.getLogger("Utilities")


def to_datetime(date_str: str) -> datetime.datetime:
    """
    Returns a datetime representation of a "%YYYY-%mm-%dd %HH:%MM:%SS" string
    """
    try:
        return datetime.datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        return None

def datetime_to_str(date_obj: datetime.datetime) -> str:
    """
    Converts a given datetime to a "%YYYY-%mm-%dd %HH:%MM:%SS" string
    """
    return date_obj.strftime('%Y-%m-%d %H:%M:%S')

def validate_request_json(req_json: dict, schema: dict) -> Union[str, int] or None:
    """
    Uses jsonschema to validate JSON requests.
    Returns None if valid; error string and status code otherwise
    """
    logger.info('Validating request JSON')
    if not req_json:
        logger.info('Invalid mediatype')
        return ['Unsupported mediatype', 415]
    try:
        validate(req_json, schema)
    except ValidationError as err:
        logger.info('Invalid JSON. %s', err)
        return f'Invalid JSON. {err}', 400
    return None
