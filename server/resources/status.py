from flask_restful import Resource

class Status(Resource):
    """
    Used to check if the API is online
    """
    def get(self):
        """
        Just returns status 200 so Hardtrace knows it can reach the API
        """
        return 'Connection working', 200
