import logging
from uuid import uuid4
from datetime import datetime
from flask import Response
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

from server import db
from server.models.user import User


logger = logging.getLogger('CreateUser')


class CreateUser(Resource):
    """
    Flask resource used to create and add new Hardtrace users to the database
    """
    def get(self):
        """
        Creates a pseudonym and UUID for a user requesting credentials.
        The created credentials are stored to the database.
        Called with GET
        """
        # Get a unique lowercase string pseudonym
        pseudonym = self.get_unique_pseudonym()
        # Get a unique authentication token
        auth_token = self.get_unique_token()
        # Successfully add to database
        new_user = User(
            created_at=datetime.now(),
            pseudonym=pseudonym,
            auth_token=auth_token
        )
        logger.info('Adding user %s to database', pseudonym)
        db.session.add(new_user)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return Response(status=409)

        # Finally, return as JSON
        return {
            'pseudonym': pseudonym,
            'auth_token': auth_token
        }, 200

    def get_unique_pseudonym(self) -> str:
        """
        Generates pseudonyms for users
        """
        pseudonym = "user" + str(User.query.count() + 1)
        return pseudonym

    def get_unique_token(self) -> str:
        """
        Generatess authentication tokens for users with uuid4
        """
        logger.info('Creating authentication token')
        auth_token = str(uuid4().hex)
        # Check just in case that not already in use
        while User.query.filter_by(auth_token=auth_token).count() > 0:
            logger.info('Not unique')
            auth_token = str(uuid4().hex)
        return auth_token
