import logging
from flask import Response
from flask_restful import Resource, request
from sqlalchemy.exc import IntegrityError, CircularDependencyError

from server import db
from server.auth import auth
from server.utilities import to_datetime, validate_request_json
from server.models.user import User
from server.models.session import Session
from server.models.process import Process

logger = logging.getLogger('Process')


class ProcessCollection(Resource):
    """
    Flask resource used to add processes to the database
    """
    def put(self, user, session_name):
        """
        Handles adding new processes to the database.
        Called when receiving a PUT request
        """
        logger.info("New processes for user %s's session %s", user, session_name)

        # Validate the provided JSON's schema
        ret = validate_request_json(request.json, Process.schema())
        if ret is not None:
            logger.info('Invalid JSON provided')
            return Response(ret[0], status=ret[1])

        # Authenticate user
        db_user = User.query.filter_by(pseudonym=user).first()
        if not auth.auth_hardtrace(db_user, request.headers.get('X-Authorization')):
            return Response('Incorrect authentication details', status=403)

        # Ensure that selected session exists
        session = Session.query.filter_by(
            user=db_user,
            name=session_name
        ).first()
        if not session:
            logger.info('No session %s for user %s', session_name, user)
            return Response(f'Session {session_name} not found', status=404)

        # Add processes to the database
        logger.info('Adding processes to the database')
        for entry in request.json:
            created_at = to_datetime(entry['created_at'])
            if created_at is None:
                logger.info('Incorrect datetime format provided')
                return Response('Incorrect datetime format', status=400)
            process = Process(
                session=session,
                created_at=to_datetime(entry['created_at']),
                hardtrace_id=entry['id'],
                pid=entry['pid']
            )
            if entry['parent_id'] == entry['id']:
                logger.info('Process can not be its own parent')
                return Response("Process can't be its own parent", status=400)
            if entry['parent_id'] != 0:
                # Connect to parent process
                db_parent_process = Process.query.filter_by(
                    session=session,
                    id=entry['parent_id']
                ).first()
                process.parent = db_parent_process
            # Add to database
            try:
                session.processes.append(process)
                db.session.add(process)
                db.session.add(session)
                db.session.commit()
            except IntegrityError:
                logger.info('Failed to add processes to the database')
                db.session.rollback()
                return Response(status=409)
            except CircularDependencyError:
                logger.info('Circular dependency error')
                db.session.rollback()
                # Is this a good idea?
                return Response(status=201)

        logger.info('Processes added')
        return Response(status=201)
