import logging
from flask import Response
from flask_restful import Resource, request
from sqlalchemy.exc import IntegrityError

from server import db
from server.auth import auth
from server.utilities import to_datetime, validate_request_json
from server.models.user import User
from server.models.session import Session
from server.models.accessed_file import AccessedFile

logger = logging.getLogger("AccessedFile")


class AccessedFileCollection(Resource):
    """
    Flask resource for adding AccessedFiles to the database
    """
    def put(self, user, session_name):
        """
        Adds new AccessedFiles to the database.
        Called with PUT
        """
        logger.info("New accessed files for user %s's session %s", user, session_name)

        # Validate the provided JSON's schema
        ret = validate_request_json(request.json, AccessedFile.schema())
        if ret is not None:
            logger.info('Invalid JSON provided')
            return Response(ret[0], status=ret[1])

        # Authorize user
        db_user = User.query.filter_by(pseudonym=user).first()
        if not auth.auth_hardtrace(db_user, request.headers.get('X-Authorization')):
            logger.info('Incorrect authentication details provided')
            return Response('Incorrect authentication details', status=403)

        # Ensure that selected session exists
        db_session = Session.query.filter_by(
            user=db_user,
            name=session_name
        ).first()
        if not db_session:
            logger.info('No session %s for user %s', session_name, user)
            return Response(f'Session {session_name} not found', status=404)

        # Add accessed files to the database
        logger.info('Adding accessed files to database')
        for entry in request.json:
            created_at = to_datetime(entry['created_at'])
            if created_at is None:
                logger.info('Incorrect datetime format provided')
                return Response('Incorrect datetime format', status=400)
            file = AccessedFile(
                session=db_session,
                created_at=created_at,
                hardtrace_id=entry["id"],
                filepath=entry["filepath"],
                pipe=entry["pipe"],
                inode=entry["inode"],
                hash=entry["hash"]
            )
            db_session.accessed_files.append(file)
            db.session.add(file)
            db.session.add(db_session)
            try:
                db.session.commit()
            except IntegrityError:
                logger.info('Failed to add files to the database')
                db.session.rollback()
                return Response(status=409)

        logger.info('Accessed files added')
        return Response(status=201)
