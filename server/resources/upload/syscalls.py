import logging
from flask import Response
from flask_restful import Resource, request
from sqlalchemy.exc import IntegrityError

from server import db
from server.auth import auth
from server.utilities import to_datetime, validate_request_json
from server.models.user import User
from server.models.session import Session
from server.models.syscall import Syscall

logger = logging.getLogger("Syscall")


class SyscallCollection(Resource):
    """
    Flask resource used to add syscalls to the database.
    """
    def put(self, user, session_name):
        """
        Add syscalls to the database.
        Called with PUT
        """
        logger.info("New syscalls for user %s's session %s", user, session_name)
        # Authenticate user
        db_user = User.query.filter_by(pseudonym=user).first()
        if not auth.auth_hardtrace(db_user, request.headers.get('X-Authorization')):
            return Response('Incorrect authentication details', status=403)

        # Validate the provided JSON's schema
        ret = validate_request_json(request.json, Syscall.schema())
        if ret is not None:
            logger.info('Invalid JSON provided')
            return Response(ret[0], status=ret[1])

        # Ensure that selected session exists
        session = Session.query.filter_by(
            user=db_user,
            name=session_name
        ).first()
        if not session:
            logger.info('No session %s for user %s', session_name, user)
            return Response(f'Session {session_name} not found', status=404)

        # Add syscalls to the database
        logger.info('Adding syscalls to database')
        for entry in request.json:
            created_at = to_datetime(entry['created_at'])
            if created_at is None:
                logger.info('Incorrect datetime format provided')
                return Response('Incorrect datetime format', status=400)
            syscall = Syscall(
                session=session,
                created_at=created_at,
                name=entry["name"],
                arg0=entry["arg0"],
            )
            # Check optional field
            if "args" in entry.keys():
                syscall.args = entry["args"]
            # Connect to accessed file
            if entry["file_id"] != 0:
                syscall.file_id = entry["file_id"]
            # Connect to process
            if entry["process_id"] != 0:
                syscall.process_id = entry["process_id"]
            # Add to database
            session.syscalls.append(syscall)
            db.session.add(syscall)
            db.session.add(session)
            try:
                db.session.commit()
            except IntegrityError:
                logger.info("Failed to store syscalls to the database")
                db.session.rollback()
                return Response(status=409)

        logger.info('Syscalls added to the database')
        return Response(status=201)
