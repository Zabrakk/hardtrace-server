import logging
from flask import Response
from datetime import datetime
from flask_restful import Resource, request
from sqlalchemy.exc import IntegrityError
# My files
from server import db
from server.auth import auth
from server.utilities import validate_request_json
from server.models.user import User
from server.models.session import Session

logger = logging.getLogger('Session')


class SessionCollection(Resource):
    """
    Flask resource used to add sessions to the database via POST.
    Can also return a list of all sessions via GET
    """
    def get(self, user):
        """
        Return all sessions of specified user in JSON format.
        Can be called by researcher
        """
        logger.info('All session of user %s requested', user)
        # Authenticate
        db_user = User.query.filter_by(pseudonym=user).first()
        if not auth.auth_hardtrace(db_user, request.headers.get('X-Authorization')):
            return Response('Incorrect authentication details', status=403)

        return [session.to_json() for session in db_user.sessions], 200

    def post(self, user):
        """
        Register a new session for a user
        """
        logger.info('User %s creating a new session', user)
        # Validate JSON
        ret = validate_request_json(request.json, Session.schema())
        if ret is not None:
            return Response(ret[0], status=ret[1])

        # Validate user's authentication
        db_user = User.query.filter_by(pseudonym=user).first()
        if not auth.auth_hardtrace(db_user, request.headers.get('X-Authorization')):
            return Response('Incorrect authentication details', status=403)

        # Check uniqueness
        name = request.json['name'].lower()
        if len(name) == 0:
            return Response('Session name is empty', 409)
        if name in [session.name for session in db_user.sessions]:
            logger.info('Session name not unique')
            return Response(f'Session {name} already exists', status=409)

        # Add to database
        session = Session(
            user=db_user,
            name=name,
            created_at=datetime.now()
        )
        db_user.sessions.append(session)
        db.session.add(db_user)
        db.session.add(session)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return Response('', status=409)

        logger.info('Session %s created', name)
        # Inform of success
        return Response(status=201, response="Session created")


class SessionItem(Resource):
    """
    Flask resource used to return a single session with GET
    """
    def get(self, user, session_name):
        """
        Returns the specified session in JSON format, if it exists.
        Only session's own user and researcher can call this
        """
        logger.info('Session %s of user %s requested', session_name, user)

        # Validate user's authentication
        db_user = User.query.filter_by(pseudonym=user).first()
        if not auth.auth_hardtrace(db_user, request.headers.get('X-Authorization')):
            return Response('Incorrect authentication details', status=403)

        # Check if session name found for specified user
        db_session = Session.query.filter_by(
            user=db_user,
            name=session_name
        ).first()
        if db_session is None:
            return Response(f"Session {session_name} doesn't exist", status=404)

        # Return session as JSON
        logger.info("Returning session as JSON")
        return db_session.to_json(), 200
