import logging
from typing import List
from flask import Response
from flask_restful import Resource, request

from server.summary import SessionSummary, FileSummary
from server.graph import Graph
from server.models.user import User
from server.models.session import Session
from server.models.accessed_file import AccessedFile
from server.utilities import validate_request_json
from server.auth import auth

logger = logging.getLogger('SummaryResource')


schema = { # Request format validation schema
    'type': 'object',
    'required': ['graph', 'includes', 'summarize'],
    'minProperties': 3,
    'maxProperties': 5,
    'properties': {
        'graph': {
            'type': 'boolean'
        },
        'includes': {
            'type': 'string',
            'enum': ['users', 'files']
        },
        'summarize': {
            'type': 'array',
            'items': {
                'minProperties': 2,
                'maxProperties': 2,
                'properties': {
                    'user': {
                        'type': 'string'
                    },
                    'session': {
                        'type': 'string'
                    },
                    'filename': {
                        'type': 'string'
                    },
                    'hash': {
                        'type': 'string',
                        'minLength': 64,
                        'MaxLength': 64
                    }
                }
            }
        }
    },
    'anyOf': [
        {
            'properties': {
                'includes': {'const': 'users'},
                'summarize': {
                    'items': {
                        'required': ['user', 'session']
                    }
                }
            }
        },
        {
            'properties': {
                'includes': {'const': 'files'},
                'summarize': {
                    'items': {
                        'required': ['filename', 'hash']
                    }
                }
            }
        }
    ]
}

class SummaryResource(Resource):
    """
    Flask resource used to provide the researcher with a summary of user sessions
    """
    def post(self):
        """
        Requests the creation of summaries based on a JSON received from the researcher.
        Called via POST
        """
        logger.info("Summary creation requested")

        # Validate provided json
        data = request.json  # type: dict
        out = validate_request_json(data, schema)
        if out is not None:
            return out[0], out[1]

        # Authenticate request
        if not auth.auth_visualizer(request.headers.get('username'), request.headers.get('X-Authorization')):
            logger.info("Invalid auth details provided")
            return Response("Incorrect authentication details provided!", status=403)

        requested_type = data['includes']
        result = [] # type: List[SessionSummary] or List[FileSummary]
        ignore_files = []
        ignore_users = []

        if 'ignore-files' in data.keys():
            ignore_files = data['ignore-files'].split(',')
        if 'ignore-users' in data.keys():
            ignore_users = data['ignore-users']

        if requested_type == 'users':
            for entry in data['summarize']:
                # Do the other stuff first
                pseudonym = entry['user']           # type: str
                session_name = entry['session']     # type: str

                # Ensure that the user-session combo exists in the database
                user = User.query.filter_by(pseudonym=pseudonym).first()                # type: User
                session = Session.query.filter_by(name=session_name, user=user).first() # type: Session
                if user is None or session is None:
                    logger.info("User & session combo: %s & %s doesn't exist", pseudonym, session_name)
                    return Response(f"User & session combo: {pseudonym} & {session_name} doesn't exist", status=404)

                summary = SessionSummary(pseudonym, session_name, session, ignore_files)
                summary.create()
                if not data['graph']:
                    result.append(summary.to_json())
                else:
                    result.append(summary)
            if data['graph']:
                graph = Graph(result)
                graph.create()
                return graph.to_json(), 200
            else:
                return result
        else: # Summarize file usage
            # Query all files that fill the user provided conditions
            if len(data['summarize']) == 0:
                return {'nodes': [], 'edges': []}, 200

            files = []  # type: List[AccessedFile]
            for entry in data['summarize']:
                db_files = AccessedFile.query.filter(
                    #AccessedFile.filepath.contains(entry['filename']),
                    AccessedFile.hash == entry['hash']
                ).all()
                for file in db_files:
                    if file not in files:
                        files.append(file)
            if len(files) == 0:
                logger.info('No files matching the user requirements found')
                return Response('No files matching your request were found', status=404)
            # Obtain all sessions that included any of the selected files
            sessions = []
            for file in files:
                if file.session not in sessions and file.session.user.pseudonym not in ignore_users:
                    sessions.append(file.session)
            summary = FileSummary(files, sessions, ignore_files)
            summary.create()

            if not data['graph']:
                return summary.to_json(), 200
            else:
                graph = Graph(summary.summaries)
                graph.create()
                graph.mark_files_as_selected(files)
                return graph.to_json(), 200
