import logging
from typing import List
from flask import Response
from flask_restful import Resource, request

from server.models.accessed_file import AccessedFile
from server.auth import auth

logger = logging.getLogger("FileInfo")


class FileInfo(Resource):
    """
    Used to provide the visualization application with a JSON of all unique files in the database
    """
    def get(self):
        """
        Returns a JSON of all non pipe files.
        Called via GET
        """
        logger.info("File info requested")

        # Authenticate request
        if not auth.auth_visualizer(request.headers.get('username'), request.headers.get('X-Authorization')):
            logger.info("Invalid auth details provided")
            return Response("Incorrect authentication details provided!", status=403)

        # Obtain all files in the database
        files = AccessedFile.query.all()  # type: List[AccessedFile]

        # Turn it into a JSON
        json = []
        names_and_hashes = []
        for file in files:
            if file.filepath.startswith("."):
                # Ignore hidden files
                continue
            file = file.to_json()
            if not file['pipe'] and file['hash'] != 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' and [file['filepath'], file['hash']] not in names_and_hashes:
                # Add all unique non-pipe, non-empty accessed files to the result
                json.append(file)
                names_and_hashes.append([file['filepath'], file['hash']])

        # Return result
        logger.info("Returning all non pipe files")
        return json, 200
