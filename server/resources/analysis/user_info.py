import logging
from typing import List
from flask import Response
from flask_restful import Resource, request

from server.models.user import User
from server.auth import auth

logger = logging.getLogger("UserInfo")


class UserInfo(Resource):
    """
    Used to provide the visualization application with a JSON of all users and their sessions
    """
    def get(self):
        """
        Returns a JSON of all users and their sessions.
        Called via GET
        """
        logger.info("User info requested")

        # Authenticate request
        if not auth.auth_visualizer(request.headers.get('username'), request.headers.get('X-Authorization')):
            logger.info("Invalid auth details provided")
            return Response("Incorrect authentication details provided!", status=403)

        # Obtain all users in the database
        users = User.query.all()  # type: List[User]

        # Turn it into a JSON
        json = {}
        for user in users:
            json[user.pseudonym] = [session.name for session in user.sessions]
        #json = [user.to_json() for user in users]

        # Return result
        logger.info("Returning users and session names")
        return json, 200
