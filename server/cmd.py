import os
import click
import logging
from flask.cli import with_appcontext

from server import db
from server.models import *
from server.constants import DB_FOLDER

logger = logging.getLogger('ModelCMD')


@click.command(name="init", help="Initializes the database")
@with_appcontext
def init_db_cmd() -> None:
    """
    Called from the command line by using "$ flask init"
    Creates an empty database containing tables for all the defined models
    """
    logger.info('Initializing the database')
    if os.getenv("USE_LOCAL_DB") == "true" and not os.path.exists(DB_FOLDER):
        os.mkdir(DB_FOLDER)
    db.drop_all()
    db.create_all()
    print('Database initialized')
