# Hardtrace Server
Server/API for [Hardtrace](https://gitlab.com/CinCan/hardtrace) data storage and analysis 

## Setup
When you first setup this project, run the following commands:
```bash
git clone https://gitlab.com/Zabrakk/hardtrace-server.git
cd hardtrace-server
python3 -m venv .
source bin/activate
pip3 install -r requirements.txt
```

Next the following environment variables need to be set
```bash
export FLASK_APP=server
export USE_LOCAL_DB="true" # or "false"
export DB_USER="<db-username>"
export DB_PASS="<db-pass>"
export DB_NAME="<db-name>"
export DB_URL="<db-url>"
export VISUALIZER_USER="<vis-user>"
export VISUALIZER_TOKEN="<vis-token>"
```

Then, you can start the server with the following commands
```bash
flask init # Create the database. NOTE: If you are connecting to an existing database, this will empty it, so don't use init in that case
flask run # Start the server
```

After a successful **flask run**, the server can be found running at http://127.0.0.1:5000/api/status/


## Data Summarization
Please refer to [Summarization.md](Summarization.md)

## Graph Data
Please refer to [Graph_Data.md](Graph_Data.md)

## Docker
Modify the environment variables in dockerfile, then run the following commands
```bash
$ docker build . -t <image name>
$ docker run --net=host <image name>
```

## Tests
To run tests locally, set the following environment variables:
```bash
export DB_USER="test"
export DB_PASS="test"
export DB_URL="127.0.0.0"
export VISUALIZER_USER="test-visualizer-username"
export VISUALIZER_TOKEN="test-visualizer-token"
export CREATE_USER_USERNAME="test-create-username"
export CREATE_USER_TOKEN="test-create-token"
```
Then use the following command:
```bash
$ python3 -m pytest -s tests
```
