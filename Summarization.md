# Session Summarization
This API is able to provide a summary of commands and modified files from Hardtrace sessions.

## An Example
Now, as an example, lets consider that the following commands were run during a Hardtrace session by the user _user1_ using the session name _test_:
```bash
$ nano test.txt # Writing test in the file
$ grep "test" test.txt -H > test2.txt
$ cat test2.txt
```

We can now obtain a summary of the session by sending the the following JSON message with a GET request to the address <**API_IP:Port**>**/api/analysis/summary/**:
```json
{
    "graph": false,
    "includes": "users",
    "summarize": [
        {
            "user": "user1",
            "session": "test"
        }
        // Note: Additional users can be added here
    ]
}
```

The API would then return a summary similar to this one:
```json
[
    {
        "pseudonym": "user1",
        "session": "test",
        "process_summary": [
            {
                "cmd": "nano test.txt",
                "cmd_type": "WRITE",
                "run_at": "2022-05-30 15:34:38",
                "old_files": [],
                "new_files": [
                    {
                        "created_at": "2022-05-30 15:34:40",
                        "filepath": "test.txt",
                        "pipe": false,
                        "inode": 15337759,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ]
            },
            {
                "cmd": "grep --color=auto test test.txt -H",
                "cmd_type": "WRITE",
                "run_at": "2022-05-30 15:34:49",
                "old_files": [
                    {
                        "created_at": "2022-05-30 15:34:49",
                        "filepath": "test.txt",
                        "pipe": false,
                        "inode": 15337759,
                        "hash": "f2ca1bb6c7e907d06dafe4687e579fce76b37e4e93b7605022da52e6ccc26fd2"
                    }
                ],
                "new_files": [
                    {
                        "created_at": "2022-05-30 15:34:49",
                        "filepath": "test2.txt",
                        "pipe": false,
                        "inode": 15337760,
                        "hash": "bf0a8a129331f72cf7e55d2dbfe9dbdeed59ff2851e7ed18c3e498c53edb7ad5"
                    }
                ]
            },
            {
                "cmd": "cat test2.txt",
                "cmd_type": "READ",
                "run_at": "2022-05-30 15:34:54",
                "old_files": [
                    {
                        "created_at": "2022-05-30 15:34:49",
                        "filepath": "test2.txt",
                        "pipe": false,
                        "inode": 15337760,
                        "hash": "bf0a8a129331f72cf7e55d2dbfe9dbdeed59ff2851e7ed18c3e498c53edb7ad5"
                    }
                ],
                "new_files": []
            }
        ]
    }
]
```
The summary includes the user's and their session's name, as well as processes that modified files. Each process includes the command run by the user in the **"cmd"** field. The **"old_filed"** field includes previously created files that the process in question used. Completely new files are included in **"new_files"**.
